package org.bitbucket.kptsui.sensorocollector;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.sensoro.beacon.kit.BeaconManagerListener;
import com.sensoro.cloud.SensoroManager;

public class App extends Application {
    public final static String TAG = "Application";
    public final static int REQUEST_ENABLE_BLUETOOTH = 111;

    private static App instance;
    // sensoroManager will not be null since it will be created first
    public SensoroManager sensoroManager;

    public final static String SN1 = "0117C5314649"; // my beacon
    public final static String SN2 = "0117C533BAC6"; // jack
    //public final static String SN4 = "0117C5310411"; // o
    public final static String SN4 = "0117C596DBEE"; // o gor box

    /*
    Application will be created first upon all activities or services
     */
    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate()");
        super.onCreate();
        instance = this;

        sensoroManager = SensoroManager.getInstance(this);
        //Enable cloud service (upload sensor data, including battery status, UMM, etc.)。Without setup, it keeps in closed status as default.
        sensoroManager.setCloudServiceEnable(false);
    }

    @Override
    public void onTerminate() {
        Log.i(TAG, "onTerminate()");
        super.onTerminate();
    }

    public void setBeaconListener(BeaconManagerListener beaconManagerListener){
        Log.i(TAG, "set Beacon Manager Listener");

        sensoroManager.stopService();
        sensoroManager.setBeaconManagerListener(beaconManagerListener);
        startService();
    }

    public void startService(){
        if(sensoroManager.isBluetoothEnabled()){
            try {
                sensoroManager.startService();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(App.instance, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    public static App getInstance(){
        return instance;
    }
}
