package org.bitbucket.kptsui.sensorocollector;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sensoro.beacon.kit.Beacon;
import com.sensoro.beacon.kit.BeaconManagerListener;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public final static String TAG = "MainActivity";

    App app;
    EditText inputX, inputY, inputFileName;
    Button btnRecord;
    TextView indicator;
    Handler mHandler;

    ArrayList<Data> list;
    boolean isRecording = false;
    String fileName;
    double x, y;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mHandler = new Handler();
        list = new ArrayList<>();
        btnRecord = (Button) findViewById(R.id.btnRecord);
        indicator = (TextView) findViewById(R.id.indicator);
        inputX = (EditText) findViewById(R.id.inputX);
        inputY = (EditText) findViewById(R.id.inputY);
        inputFileName = (EditText) findViewById(R.id.inputFileName);
        app = App.getInstance();
        app.setBeaconListener(new BeaconManagerListener() {
            @Override
            public void onUpdateBeacon(ArrayList<Beacon> beacons) {
                if(!isRecording){
                    Log.i(TAG, "!isRecording");
                    return;
                }
                // 传感器信息更新
                Log.i(TAG, "Update Beacon, list size: " + list.size());

                if(list.size() >= 150){
                    isRecording = false;
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            btnRecord.setText("Start Record");
                            Toast.makeText(app, "Record finished", Toast.LENGTH_LONG).show();

                            writeCSV(null);
                            indicator.setText("0");
                        }
                    });
                    return;
                }
                try {
                    final Beacon[] arr = beacons.toArray(new Beacon[beacons.size()]);
                    sortBeacons(arr);
                    if(arr.length <= 2){
                        return ;
                    }
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            double x = Double.parseDouble(inputX.getText().toString());
                            double y = Double.parseDouble(inputY.getText().toString());

                            for(int i = 0; i < 3; i++){
                                list.add(new Data(x, y, arr[i].getRssi(), arr[i].getAccuracy(), arr[i].getSerialNumber()));
                            }
                            indicator.setText(String.valueOf(list.size()));
                        }
                    });

                } catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(app, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNewBeacon(Beacon beacon) {
                Log.i(TAG, "New Beacon Found - ID: " + beacon.getEddystoneUID() +
                        "\nUrl: " + beacon.getEddystoneURL());

            }

            @Override
            public void onGoneBeacon(Beacon beacon) {
                Log.i(TAG, "Beacon Gone - ID: " + beacon.getEddystoneUID() +
                        "\nUrl: " + beacon.getEddystoneURL());
            }
        });
    }

    public void sortBeacons(Beacon[] beacons){
        int n = beacons.length;
        Beacon temp = null;

        for(int i=0; i < n; i++){
            for(int j=1; j < (n-i); j++){
                if(beacons[j-1].getAccuracy() > beacons[j].getAccuracy()){
                    //swap the elements!
                    temp = beacons[j-1];
                    beacons[j-1] = beacons[j];
                    beacons[j] = temp;
                }

            }
        }
    }

    public void startRecord(View v){
        if(inputFileName.getText().toString().isEmpty()
                || inputX.getText().toString().isEmpty()
                || inputY.getText().toString().isEmpty()){
            Toast.makeText(app, "Input all fields!!", Toast.LENGTH_LONG).show();
            return;
        }
        fileName = inputFileName.getText().toString() + ".csv";;
        x = Double.parseDouble(inputX.getText().toString());
        y = Double.parseDouble(inputY.getText().toString());

        isRecording = true;
        btnRecord.setText("Recording....");
    }

    public void writeCSV(View v){
        if(fileName.isEmpty()){
            return;
        }

        String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/CSV";
        File folder = new File(dir);
        folder.mkdirs();

        File file = new File(folder, fileName);

        Log.i(TAG, "saving file: " + fileName);
        try {
            FileOutputStream out = new FileOutputStream(file, true); // append

            out.write("X,Y,Rssi,Distance,ID".getBytes());

            for (Data data : list){
                out.write(data.toString().getBytes());
            }
            out.close();
            Log.i(TAG, "FileOutputStream close");

            // saving the image file
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(Uri.fromFile(file));
            MainActivity.this.sendBroadcast(mediaScanIntent);

            Toast.makeText(app, "Saved", Toast.LENGTH_LONG).show();
            list = new ArrayList<>();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Save file failed");
        }

    }
}
