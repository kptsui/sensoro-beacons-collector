package org.bitbucket.kptsui.sensorocollector;

/**
 * Created by user on 26/10/2016.
 */

public class Data {
    public double x;
    public double y;
    public double rssi;
    public double distance;
    public String id;

    public Data(double x, double y, double rssi, double distance, String fromBeaconSN){
        this.x = x;
        this.y = y;
        this.rssi = rssi;
        this.id = fromBeaconSN;
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "\n" + x +
                "," + y +
                "," + rssi +
                "," + distance +
                "," + id;
    }
}
